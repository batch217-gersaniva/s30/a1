// 2. Use the count operator to count the total number of fruits on sale.


db.fruits.aggregate([
{ $match: {onSale: true} },
{ $count: "Fruits on Sale"},

]);



// # 3. Use the count operator to count the total number of fruits with stock more than 20.



db.fruits.aggregate ([
	{ $match: {stock: {$gt :20} } },
	{ $count: "Fruits with stock more than 20"}
]);


// # 4. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate ([
	{ $match: {onSale: true}},
	{ $group: {_id: "$supplier_id", onSale_fruits_avgprice_perSupplier: {$avg: "$price"} } }
]);

// # 5. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate ([
	{ $match: {onSale: true}},
	{ $group: {_id: "$supplier_id", supplier_fruits_maxprice: {$max: "$price"} } }
]);



// # 6. Use the min operator to get the lowest price of a fruit per supplier.



db.fruits.aggregate ([
	{ $match: {onSale: true}},
	{ $group: {_id: "$supplier_id", supplier_fruits_maxprice: {$min: "$price"} } }
]);



































